var matrix, item_col, container, form_matrix, matrice, elm_div_btn, graphTemplate = null, textarea;


function focused(){

    container = document.getElementById('container')
    matrix = document.getElementById('wrapper')
    item_col = document.getElementById('item-col');

    item_col.value = "0"
}

function resetMatrix(){
    container.classList.add('unActive')
    item_col.value = "0"
    if(form_matrix != undefined)
        resetNode(form_matrix);
}


function submitData(){
    var item_col_value = item_col.value
    if((!isNaN(item_col_value))) {
        if(Number(item_col_value) <3){
            alert('Renseigner une dimension correcte pour la matrice !')
            return false
        }else if(Number(item_col_value) > 10){
            alert('Votre matrixe est trop grande taille < 5!')
            return false
        }
    }else{
        alert('Vous devez saisir une valeur numerique')
        return false;
    }
    drawMatrix(item_col_value)
    container.classList.remove('unActive')
}

/**
 * 
 * @param {number} cols 
 * @param {number} rows 
 */
function drawMatrix(rows){
    resetNode(form_matrix); // suppression du noeid  chaque crcreationeaition
    form_matrix = document.getElementById('wrapper')
    var i, j, elm_div, elm_input, elm_submit_btn;

    matrice = new Array();
    matrice.length = parseInt(rows);

    for(i=0; i<rows; i++){
        matrice[i] = new Array();
        matrice[i].length = parseInt(rows);
        for(j=0; j<rows; j++){

            elm_div = document.createElement("div");
            
            if(i===0 && j===0){
                elm_div.style.backgroundColor = 'gray'
            }else{
                elm_input = document.createElement("input");
                elm_input.setAttribute("type", "text");
                
                elm_div.classList.add('box');
                
                elm_div.style.gridColumnStart = j+1;
                elm_div.style.gridColumnEnd = j+2;
                elm_div.style.gridRowStart = i+1;
                elm_div.style.gridRowEnd   = i+2;
                
                addDefaultText(i, j, elm_input)
                
                elm_div.append(elm_input);

                // ajout des classe customisees
                if(i===0 || j===0) addSomeClass(i, j, elm_div, elm_input)
            }
            form_matrix.append(elm_div);
            matrice[i][j] = elm_div
        }
    }
    elm_submit_btn = document.createElement("button");
    elm_submit_btn.innerHTML = "Construire";
    elm_submit_btn.type = "button";
    elm_submit_btn.classList.add('construire');
    elm_submit_btn.addEventListener('click', function(){
        generateGraph(matrice)
    })

    
    elm_div_btn = document.createElement("div");
    elm_div_btn.append(elm_submit_btn);
    
    container.append(elm_div_btn);

}

    function onkeyUP(ii, jj) {
        var correspondant = matrice[jj][ii]
        var items = matrice[ii][jj]
        var inputs = correspondant.getElementsByTagName('input')
        var inputs2 = items.getElementsByTagName('input')
        inputs[0].value = inputs2[0].value
       
        // var i, j;
        // for(i=0; i<matrice.length; i++){
        //     for(j=0; j<matrice.length; j++){
        //         if(i===0 || j===0){
        //             if()
        //         }
        //     }
        // }
    }

    /**
     * 
     * @param {number} i 
     * @param {number} j 
     * @param {object} elm_input 
     */
    function addDefaultText(i, j, elm_input){
        if(j == 0 || i == 0){
            elm_input.setAttribute("placeholder", "Node");
            elm_input.style.backgroundColor = 'rgb(255, 179, 0)'
        }else{
            elm_input.setAttribute("placeholder", "Value");
        }
    }

    /**
     * 
     * @param {number} i 
     * @param {number} j 
     * @param {object} elm_div 
     */
    function addSomeClass(i, j, elm_div, elm_input){
        //est un titre
        elm_div.classList.add('case-titre');
        
        if(j===0){
            // titre sur les lignes
            elm_div.classList.add('case-titre-i-' + (i+1));
            elm_input.readOnly = true
        }else if(i===0){
            // titre sur les colonnes
            elm_div.classList.add('case-titre-j-' + (j+1));

            // ajout de l'evennement 
            elm_input.addEventListener("keyup", function(){
                onkeyUP(i,j)
            });
        }
    }

    /**
     * 
     * @param {object} node 
     */
    function resetNode(node){
        if(node != undefined)
            node.replaceChildren();
        if(elm_div_btn != undefined)
            container.removeChild(elm_div_btn)

    }


    // ######################################################################################
    // 
    //                                  GO JS
    // 
    // ######################################################################################




    function init() {
        if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates
    
        myDiagram =
          $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
              // have mouse wheel events zoom in and out instead of scroll up and down
              "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
              // support double-click in background creating a new node
              "clickCreatingTool.archetypeNodeData": { text: "new node" },
              // enable undo & redo
              "undoManager.isEnabled": true
            });
    
        // install the NodeLabelDraggingTool as a "mouse move" tool
        myDiagram.toolManager.mouseMoveTools.insertAt(0, new NodeLabelDraggingTool());
    
        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
          var button = document.getElementById("SaveButton");
          if (button) button.disabled = !myDiagram.isModified;
          var idx = document.title.indexOf("*");
          if (myDiagram.isModified) {
            if (idx < 0) document.title += "*";
          } else {
            if (idx >= 0) document.title = document.title.slice(0, idx);
          }
        });
    
        // define the Node template
        myDiagram.nodeTemplate =
          $(go.Node, "Spot",
            { locationObjectName: "ICON", locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            { selectionObjectName: "ICON" },
            // define the node primary shape
            $(go.Shape, "RoundedRectangle",
              {
                name: "ICON",
                parameter1: 10,  // the corner has a medium radius
                desiredSize: new go.Size(40, 40),
                fill: $(go.Brush, "Linear", { 0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)" }),
                stroke: "black",
                portId: "",
                fromLinkable: true,
                fromLinkableSelfNode: true,
                fromLinkableDuplicates: true,
                toLinkable: true,
                toLinkableSelfNode: true,
                toLinkableDuplicates: true,
                cursor: "pointer"
              }),
            $(go.Shape,  // provide interior area where the user can grab the node
              { fill: "transparent", stroke: null, desiredSize: new go.Size(30, 30) }),
            $(go.TextBlock,
              {
                font: "bold 11pt helvetica, bold arial, sans-serif",
                editable: true,  // editing the text automatically updates the model data
                _isNodeLabel: true,
                cursor: "move"  // visual hint that the user can do something with this node label
              },
              new go.Binding("text", "text").makeTwoWay(),
              // The GraphObject.alignment property is what the NodeLabelDraggingTool modifies.
              // This TwoWay binding saves any changes to the same named property on the node data.
              new go.Binding("alignment", "alignment", go.Spot.parse).makeTwoWay(go.Spot.stringify)
            )
          );
    
        // unlike the normal selection Adornment, this one includes a Button
        myDiagram.nodeTemplate.selectionAdornmentTemplate =
          $(go.Adornment, "Spot",
            $(go.Panel, "Auto",
              $(go.Shape, { fill: null, stroke: "blue", strokeWidth: 2 }),
              $(go.Placeholder)  // this represents the selected Node
            ),
            // the button to create a "next" node, at the top-right corner
            $("Button",
              {
                alignment: go.Spot.TopRight,
                click: addNodeAndLink  // this function is defined below
              },
              $(go.Shape, "PlusLine", { desiredSize: new go.Size(6, 6) })
            ) // end button
          ); // end Adornment
    
        // clicking the button inserts a new node to the right of the selected node,
        // and adds a link to that new node
        function addNodeAndLink(e, obj) {
          var adorn = obj.part;
          e.handled = true;
          var diagram = adorn.diagram;
          diagram.startTransaction("Add State");
    
          // get the node data for which the user clicked the button
          var fromNode = adorn.adornedPart;
          var fromData = fromNode.data;
          // create a new "State" data object, positioned off to the right of the adorned Node
          var toData = { text: "new" };
          var p = fromNode.location.copy();
          p.x += 200;
          toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
          // add the new node data to the model
          var model = diagram.model;
          model.addNodeData(toData);
    
          // create a link data from the old node data to the new node data
          var linkdata = {
            from: model.getKeyForNodeData(fromData),  // or just: fromData.id
            to: model.getKeyForNodeData(toData),
            text: "transition"
          };
          // and add the link data to the model
          model.addLinkData(linkdata);
    
          // select the new Node
          var newnode = diagram.findNodeForData(toData);
          diagram.select(newnode);
    
          diagram.commitTransaction("Add State");
    
          // if the new node is off-screen, scroll the diagram to show the new node
          diagram.scrollToRect(newnode.actualBounds);
        }
    
        // replace the default Link template in the linkTemplateMap
        myDiagram.linkTemplate =
          $(go.Link,  // the whole link panel
            { curve: go.Link.Bezier, adjusting: go.Link.Stretch, reshapable: true },
            new go.Binding("points").makeTwoWay(),
            new go.Binding("curviness", "curviness"),
            $(go.Shape,  // the link shape
              { strokeWidth: 1.5 }),
            $(go.Shape,  // the arrowhead
              { toArrow: "standard", stroke: null }),
            $(go.Panel, "Auto",
              $(go.Shape,  // the label background, which becomes transparent around the edges
                {
                  fill: $(go.Brush, "Radial",
                    { 0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)" }),
                  stroke: null
                }),
              $(go.TextBlock, "transition",  // the label text
                {
                  textAlign: "center",
                  font: "10pt helvetica, arial, sans-serif",
                  stroke: "black",
                  margin: 4,
                  editable: true  // editing the text automatically updates the model data
                },
                new go.Binding("text", "text").makeTwoWay())
            )
          );
    
        // read in the JSON-format data from the "mySavedModel" element
        load();
      }

      
    
      // Show the diagram's model in JSON format
      function save() {
        document.getElementById("mySavedModel").value = myDiagram.model.toJson();
        console.log(document.getElementById("mySavedModel").value);
        myDiagram.isModified = false;
      }
      function load() {
        myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
      };

      /**
       * 
       * @param {object} matrice 
       */
      function generateGraph(matrice){
        let i, j,k=0, 
        graphTemplate = { 
            class: "go.GraphLinksModel",
            nodeKeyProperty: "id",
            nodeDataArray: [],
            linkDataArray: [],
        };
        const posti = [120, 330, 236, 60, 226]
        const postj = [120, 120, 376, 276, 226]
        let toggle = true;
        textarea = document.getElementById('mySavedModel')

        for(i=0; i<matrice.length; i++){
            for(j=0; j<matrice.length; j++){

                // recuperation des valeure des titres
                if(k<(matrice.length -1) && i==0 && j>0){
                    graphTemplate.nodeDataArray.push({ "id": k, "loc": posti[k]+" "+postj[k], "text": matrice[0][j].getElementsByTagName('input')[0].value })
                    k++;
                }else if(i> 0 && i==j){
                    // recuperation des element en position i, j (i==j)
                    if(toggle){
                        graphTemplate.linkDataArray.push({ "from": (i-1), "to": (i-1), "text": matrice[i][i].getElementsByTagName('input')[0].value, "curviness": -20 })
                    }else{
                        graphTemplate.linkDataArray.push({ "from": (i-1), "to": (i-1), "text": matrice[i][i].getElementsByTagName('input')[0].value, "curviness": 20 })
                    }
                    toggle = !toggle;
                }else if(i> 0 && j>0){
                    // recuperation des element en position i, j (i!=j)
                    graphTemplate.linkDataArray.push({ "from": (i-1), "to": (j-1), "text": matrice[i][j].getElementsByTagName('input')[0].value })
                }
            }
        }


        // node.replaceChildren();
        let textAreaContent = JSON.stringify(graphTemplate)
        // let h =0, p;
        // let firstSTR = '', endSTR = '';


        // for(h=0; h<textAreaContent.length; h++){
        //     firstSTR = firstSTR + textAreaContent[h]
        //     if(textAreaContent[h] == '[' || textAreaContent[h] == ',' || (textAreaContent[h] == '}' && textAreaContent[h+1] != ',')){
        //         for(p=(h+1); p<textAreaContent.length; p++){ 
        //             endSTR = endSTR + textAreaContent[p];
        //         }
        //         textAreaContent = firstSTR + '<br/>' + endSTR;
        //     }
            
        // }
        // console.log('fin...');
        textarea.innerHTML = textAreaContent; // remplacement de la valur demande r dans l ehtml du textare

        load()

      };
      window.addEventListener('DOMContentLoaded', init);

